﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameDirector : MonoBehaviour
{
    public GridDirector gridDirector;
    public CharacterDirector characterDirector;
    public InstructionSetDirector instructionSetDirector;
    public CodeDirector codeDirector;
    public MainMenuDirector mainMenuDirector;
    public CodeToMovementInstructionsConverter codeToMovementInstructionsConverter;
    public AudioDirector audioDirector;
    public CountInputFieldManager countInputFieldManager;

    [Space(20)]
    public int timer;

    internal GAME_STATE gameState = GAME_STATE.MAIN_MENU;
    internal int score;

    public void FirstTimeLoad()
    {
        score = 0;
        gridDirector.LoadLevel();
        codeDirector.LoadLevel();
        instructionSetDirector.LoadLevel();
        characterDirector.LoadLevel();
        codeToMovementInstructionsConverter.LoadLevel();
        codeToMovementInstructionsConverter.ConvertCodeToMovementInstructions(codeDirector.GetCurrentCode(), false);
    }

    public void LoadLevel()
    {
        countInputFieldManager.LoadLevel();
        gridDirector.LoadNewTargetPoint();
        instructionSetDirector.RemoveHighlightFromAllInstructionSet();
        gameState = GAME_STATE.GAME;
    }

    public void AchieveTarget()
    {
        LoadLevel();
        audioDirector.PlaySFX(AUDIO_SFX_EVENTS.LEVEL_WON);
    }

    public void GameOver()
    {
        audioDirector.PlaySFX(AUDIO_SFX_EVENTS.GAME_OVER);
        gameState = GAME_STATE.MAIN_MENU;
        mainMenuDirector.gameObject.SetActive(true);
        mainMenuDirector.transform.DOLocalMoveY(0, 0.25f);
        mainMenuDirector.GameOver(score);
    }
}
