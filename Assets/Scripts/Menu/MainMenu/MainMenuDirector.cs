﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenuDirector : MonoBehaviour
{
    public GameDirector gameDirector;
    public AudioDirector audioDirector;
    public Text scoreText;
    public GameObject tutorialScreen;

    private bool isTutorialOpen;

    private void PlayButtonCallback()
    {
        audioDirector.PlaySFX(AUDIO_SFX_EVENTS.CODE_WINDOW_INTERACTION);
        transform.DOLocalMoveY(1000, 1f).OnComplete(() =>
       {
           gameDirector.FirstTimeLoad();
           gameDirector.LoadLevel();
           gameObject.SetActive(false);
       });
    }

    private void Update()
    {
        if (gameDirector.gameState != GAME_STATE.MAIN_MENU)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Invoke("PlayButtonCallback", 0.2f);
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.CODE_WINDOW_INTERACTION);
            isTutorialOpen = !isTutorialOpen;
            tutorialScreen.SetActive(isTutorialOpen);
        }
    }

    public void GameOver(int score)
    {
        scoreText.gameObject.SetActive(true);
        scoreText.text = "SCORE: " + score.ToString();
    }
}
