﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementManager : MonoBehaviour
{
    public CharacterDirector characterDirector;

    public SpriteRenderer characterSprite;
    public Sprite left, right, up, down;

    public void MoveCharacter(MOVEMENT_DIRECTION direction)
    {
        Vector3 pos = transform.position;
        switch (direction)
        {
            case MOVEMENT_DIRECTION.BOTTOM:
                {
                    pos.y -= 1;
                    break;
                }
            case MOVEMENT_DIRECTION.TOP:
                {
                    pos.y += 1;
                    break;
                }
            case MOVEMENT_DIRECTION.RIGHT:
                {
                    pos.x += 1;
                    break;
                }
            case MOVEMENT_DIRECTION.LEFT:
                {
                    pos.x -= 1;
                    break;
                }
        }
        pos.x = Mathf.Clamp(pos.x, -2, 4);
        pos.y = Mathf.Clamp(pos.y, -4, 2);
        transform.position = pos;
    }

    public void UpdateCharacterSprite(MOVEMENT_DIRECTION direction)
    {
        switch (direction)
        {
            case MOVEMENT_DIRECTION.TOP:
                {
                    characterSprite.sprite = up;
                    break;
                }
            case MOVEMENT_DIRECTION.BOTTOM:
                {
                    characterSprite.sprite = down;
                    break;
                }
            case MOVEMENT_DIRECTION.LEFT:
                {
                    characterSprite.sprite = left;
                    break;
                }
            case MOVEMENT_DIRECTION.RIGHT:
                {
                    characterSprite.sprite = right;
                    break;
                }
        }
    }
}
