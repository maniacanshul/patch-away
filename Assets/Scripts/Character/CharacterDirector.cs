﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterDirector : MonoBehaviour
{
    public AudioDirector audioDirector;
    public GridDirector gridDirector;
    public GameDirector gameDirector;
    public CodeToMovementInstructionsConverter codeToMovementInstructionsConverter;

    internal bool isCharacterMoving;

    private CharacterMovementManager movementManager;

    private Vector2 resetPostion;
    private Coroutine characterMovementCoroutine;

    private void Awake()
    {
        isCharacterMoving = false;
        movementManager = GetComponent<CharacterMovementManager>();
        resetPostion = transform.position;
    }

    public void LoadLevel()
    {
        transform.position = resetPostion;
        movementManager.UpdateCharacterSprite(MOVEMENT_DIRECTION.RIGHT);
    }

    public void ResetLevel()
    {
        if (characterMovementCoroutine != null)
        {
            StopCoroutine(characterMovementCoroutine);
        }

        transform.position = resetPostion;
    }

    public void HitAObstacle()
    {
        isCharacterMoving = false;
        if (characterMovementCoroutine != null)
        {
            StopCoroutine(characterMovementCoroutine);
        }
    }

    public void UploadCharacterMovement(List<MovementData> movementData)
    {
        if (characterMovementCoroutine != null)
        {
            StopCoroutine(characterMovementCoroutine);
        }
        characterMovementCoroutine = StartCoroutine(CharacterMovementCoroutineIE(movementData));
    }

    private IEnumerator CharacterMovementCoroutineIE(List<MovementData> movementData)
    {
        isCharacterMoving = true;
        yield return new WaitForSeconds(1.1f);
        transform.DOPunchScale(Vector3.one * 0.45f, 0.15f);
        for (int i = 0; i < movementData.Count; i++)
        {
            yield return new WaitForSeconds(0.1f);

            if (movementData[i].isMovement)
            {
                bool foundObstacle = gridDirector.CheckIfNextGridIsObstacleForCharacter(movementData[i].direction);
                yield return new WaitForSeconds(0.15f);
                if (!foundObstacle)
                {
                    movementManager.MoveCharacter(movementData[i].direction);
                    audioDirector.PlaySFX(AUDIO_SFX_EVENTS.CHARACTER_MOVEMENT);
                }
                else
                {
                    codeToMovementInstructionsConverter.currentForward = movementData[i].direction;
                    HitAObstacle();
                    yield return null;
                }
            }
            else
            {
                movementManager.UpdateCharacterSprite(movementData[i].direction);
                audioDirector.PlaySFX(AUDIO_SFX_EVENTS.CHARACTER_ROTATION);
            }
        }

        yield return new WaitForSeconds(0.25f);
        isCharacterMoving = false;
        Vector2 tempPos = transform.position;
        if (Vector2.Distance(tempPos, gridDirector.GetTargetPointCoordinates()) < 0.25f)
        {
            gameDirector.AchieveTarget();
            transform.DOKill();
            transform.DOPunchScale(Vector3.one * 0.45f, 0.15f);
        }
    }
}
