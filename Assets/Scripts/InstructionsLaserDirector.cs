﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class InstructionsLaserDirector : MonoBehaviour
{
    public Camera gameCam;
    public Transform playerCharacter;
    public Transform binaryImage, binaryImageCopy;
    public List<GameObject> flyingText;

    public void DrawLasers()
    {
        StartCoroutine(BlinkLasers());
    }

    private IEnumerator BlinkLasers()
    {
        int loopCounter = 0;
        while (loopCounter < 3)
        {
            for (int i = 0; i < flyingText.Count; i++)
            {
                flyingText[i].SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < flyingText.Count; i++)
            {
                flyingText[i].SetActive(false);
            }
            yield return new WaitForSeconds(0.1f);
            loopCounter++;
        }
        MoveToCharacter();
    }

    private void MoveToCharacter()
    {
        binaryImageCopy.gameObject.SetActive(true);
        binaryImageCopy.transform.position = binaryImage.transform.position;
        binaryImageCopy.transform.localScale = Vector3.one;

        Vector3 endPoint = gameCam.WorldToScreenPoint(playerCharacter.position);

        binaryImageCopy.transform.DOMove(endPoint, 0.45f);
        binaryImageCopy.transform.DOScale(Vector3.zero, 0.45f).OnComplete(() =>
       {
           binaryImageCopy.gameObject.SetActive(false);
       });
    }
}
