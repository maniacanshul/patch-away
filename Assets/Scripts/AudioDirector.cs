﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDirector : MonoBehaviour
{
    public List<AudioSFXData> audioEventData;
    public List<AudioSource> audioSources;

    private List<AudioSource> beingUsedAudioSources;

    private void Start()
    {
        beingUsedAudioSources = new List<AudioSource>();
    }

    public void PlaySFX(AUDIO_SFX_EVENTS sfxEvent)
    {
        for (int i = 0; i < audioEventData.Count; i++)
        {
            if (audioEventData[i].audioEvent == sfxEvent)
            {
                PlaySfxClip(audioEventData[i].clip);
                break;
            }
        }
    }

    private void PlaySfxClip(AudioClip audio)
    {
        AudioSource temp = audioSources[0];
        temp.clip = audio;
        temp.Play();
        audioSources.Remove(temp);
        beingUsedAudioSources.Add(temp);
        PoolBackAudioSources();
    }

    private void PoolBackAudioSources()
    {
        AudioSource temp;
        for (int i = beingUsedAudioSources.Count - 1; i >= 0; i--)
        {
            temp = beingUsedAudioSources[i];
            if (!temp.isPlaying)
            {
                beingUsedAudioSources.Remove(temp);
                audioSources.Add(temp);
            }
        }
    }

    [System.Serializable]
    public class AudioSFXData
    {
        public AUDIO_SFX_EVENTS audioEvent;
        public AudioClip clip;
    }
}
