﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CodeManager : MonoBehaviour
{
    public Text functionText;
    public Image codeBlockImage;

    private FUNCTIONS currentFunction;
    private int currentRepetition;

    public void SetupFunction(FUNCTIONS function, int repetition)
    {
        currentFunction = function;
        currentRepetition = repetition;
        if (function != FUNCTIONS.NULL)
        {
            functionText.text = function.ToString() + "(" + repetition.ToString() + ")";
        }
        else
        {
            functionText.text = "";
        }
    }

    public void HighlightCodeBlock(bool highlight)
    {
        codeBlockImage.DOFade(highlight ? 1 : 0, 0);
    }

    public FUNCTIONS GetCurrentFunction()
    {
        return currentFunction;
    }

    public int GetCurrentRepetition()
    {
        return currentRepetition;
    }
}
