﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodeDirector : MonoBehaviour
{
    public GameDirector gameDirector;
    public AudioDirector audioDirector;
    public Image backgroundImage;
    public Color deactivatedColor;
    public List<CodeManager> codeManagers;

    private List<CodeData> currentCode;
    private List<CodeData> initialCode;

    private CodeData codeDataFiller;
    private bool canNavigate;
    private int currentIndexAt;

    public void LoadLevel()
    {
        currentCode = new List<CodeData>();
        initialCode = new List<CodeData>();
        codeDataFiller = new CodeData();

        for (int i = 0; i < codeManagers.Count; i++)
        {
            codeManagers[i].HighlightCodeBlock(false);
        }
        currentIndexAt = 0;
        canNavigate = true;
        codeManagers[0].HighlightCodeBlock(true);
        GenerateGiberishCode();
    }

    private void GenerateGiberishCode()
    {
        initialCode.Clear();

        for (int i = 0; i < Random.Range(2, 6); i++)
        {
            codeDataFiller.function = (FUNCTIONS)Random.Range(0, 3);
            codeDataFiller.repetition = Random.Range(1, 3);
            initialCode.Add(codeDataFiller);
            codeManagers[i].SetupFunction(codeDataFiller.function, codeDataFiller.repetition);
        }
    }

    public void ResetLevel()
    {
        for (int i = 0; i < codeManagers.Count; i++)
        {
            codeManagers[i].SetupFunction(FUNCTIONS.NULL, 0);
            if (i < initialCode.Count)
            {
                codeManagers[i].SetupFunction(initialCode[i].function, initialCode[i].repetition);
            }
        }
    }

    public void ToggleActive(bool navigate)
    {
        canNavigate = navigate;
        backgroundImage.color = canNavigate ? Color.black : deactivatedColor;
    }

    private void Update()
    {
        if (!canNavigate || gameDirector.gameState != GAME_STATE.GAME)
        {
            return;
        }

        int lastIndexAt = currentIndexAt;

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.ARROW_KEYS);
            currentIndexAt++;
            if (currentIndexAt >= codeManagers.Count)
            {
                currentIndexAt = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.ARROW_KEYS);
            currentIndexAt--;
            if (currentIndexAt < 0)
            {
                currentIndexAt = codeManagers.Count - 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Backspace))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.FUNCTION_DELETE);
            codeManagers[currentIndexAt].SetupFunction(FUNCTIONS.NULL, 0);
            
        }

        if (lastIndexAt != currentIndexAt)
        {
            HighlightCodeBlock();
        }
    }

    private void HighlightCodeBlock()
    {
        for (int i = 0; i < codeManagers.Count; i++)
        {
            codeManagers[i].HighlightCodeBlock(i == currentIndexAt);
        }
    }

    public void SetupNewFunction(FUNCTIONS function, int repetition)
    {
        codeManagers[currentIndexAt].SetupFunction(function, repetition);
    }

    public List<CodeData> GetCurrentCode()
    {
        currentCode.Clear();
        for (int i = 0; i < codeManagers.Count; i++)
        {
            codeDataFiller.function = codeManagers[i].GetCurrentFunction();
            codeDataFiller.repetition = codeManagers[i].GetCurrentRepetition();
            currentCode.Add(codeDataFiller);
        }
        return currentCode;
    }
}
