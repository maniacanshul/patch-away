﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeToMovementInstructionsConverter : MonoBehaviour
{
    internal MOVEMENT_DIRECTION currentForward;
    
    private List<MovementData> movementInstructions;
    private MovementData movementDataFiller;

    private void Start()
    {
        movementInstructions = new List<MovementData>();
        movementDataFiller = new MovementData();
        currentForward = MOVEMENT_DIRECTION.RIGHT;
    }

    public void LoadLevel()
    {
        currentForward = MOVEMENT_DIRECTION.RIGHT;
    }

    public void ConvertCodeToMovementInstructions(List<CodeData> codeData, bool updateState)
    {
        movementInstructions.Clear();
        // currentForward = MOVEMENT_DIRECTION.RIGHT;
       MOVEMENT_DIRECTION tempCurrentForward = currentForward; 
        for (int i = 0; i < codeData.Count; i++)
        {
            for (int j = 0; j < codeData[i].repetition; j++)
            {
                movementDataFiller.isMovement = false;
                switch (codeData[i].function)
                {
                    case FUNCTIONS.WALK:
                        {
                            movementDataFiller.isMovement = true;
                            break;
                        }
                    case FUNCTIONS.TURN_LEFT:
                        {
                            switch (currentForward)
                            {
                                case MOVEMENT_DIRECTION.TOP:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.LEFT;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.LEFT:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.BOTTOM;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.BOTTOM:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.RIGHT;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.RIGHT:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.TOP;
                                        break;
                                    }
                            }
                            break;
                        }
                    case FUNCTIONS.TURN_RIGHT:
                        {
                            switch (currentForward)
                            {
                                case MOVEMENT_DIRECTION.TOP:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.RIGHT;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.RIGHT:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.BOTTOM;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.BOTTOM:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.LEFT;
                                        break;
                                    }
                                case MOVEMENT_DIRECTION.LEFT:
                                    {
                                        currentForward = MOVEMENT_DIRECTION.TOP;
                                        break;
                                    }
                            }
                            break;
                        }
                }
                movementDataFiller.direction = currentForward;
                movementInstructions.Add(movementDataFiller);
            }
        }
        if(!updateState)
        {
            currentForward = tempCurrentForward;
        }
    }

    public List<MovementData> GetLastUploadedCode()
    {
        return movementInstructions;
    }
}
