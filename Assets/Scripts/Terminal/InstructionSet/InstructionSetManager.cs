﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class InstructionSetManager : MonoBehaviour
{
    public Text instructionSetText;
    public Image instructionSetImage;

    private FUNCTIONS currentFunction;

    public void SetupFunctions(FUNCTIONS function)
    {
        currentFunction = function;
        instructionSetText.text = function.ToString() + "()";
    }

    public void HighLightInstructionSetImage(bool highlight)
    {
        instructionSetImage.DOFade(highlight ? 1 : 0, 0);
    }

    public FUNCTIONS GetFunctionData()
    {
        return currentFunction;
    }
}
