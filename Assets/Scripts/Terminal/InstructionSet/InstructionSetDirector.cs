﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class InstructionSetDirector : MonoBehaviour
{
    public GameDirector gameDirector;
    public AudioDirector audioDirector;
    public Image backgroundImage;
    public Color deactivatedColor;
    public List<InstructionSetManager> instructionSetManagers;

    internal bool dontNavigateWhenInputtingNumbers;
    internal int currentIndexAt = 0;

    private bool canNavigate;

    public void LoadLevel()
    {
        for (int i = 0; i < instructionSetManagers.Count; i++)
        {
            instructionSetManagers[i].SetupFunctions((FUNCTIONS)i);
        }
    }

    public void ToggleActive(bool navigate)
    {
        canNavigate = navigate;

        RemoveHighlightFromAllInstructionSet();
        currentIndexAt = 0;
        dontNavigateWhenInputtingNumbers = canNavigate;
        if (canNavigate)
        {
            HighlightFirstInstructionSet();
        }
        backgroundImage.color = canNavigate ? Color.black : deactivatedColor;
    }

    private void Update()
    {
        if (!canNavigate || gameDirector.gameState != GAME_STATE.GAME || !dontNavigateWhenInputtingNumbers)
        {
            return;
        }

        int lastIndexAt = currentIndexAt;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.ARROW_KEYS);
            currentIndexAt--;
            if (currentIndexAt < 0)
            {
                currentIndexAt = instructionSetManagers.Count - 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.ARROW_KEYS);
            currentIndexAt++;
            if (currentIndexAt >= instructionSetManagers.Count)
            {
                currentIndexAt = 0;
            }
        }

        if (lastIndexAt != currentIndexAt)
        {
            HighLightParticularInstructionSet();
        }
    }

    private void HighLightParticularInstructionSet()
    {
        for (int i = 0; i < instructionSetManagers.Count; i++)
        {
            instructionSetManagers[i].HighLightInstructionSetImage(i == currentIndexAt);
        }
    }

    public void RemoveHighlightFromAllInstructionSet()
    {
        for (int i = 0; i < instructionSetManagers.Count; i++)
        {
            instructionSetManagers[i].HighLightInstructionSetImage(false);
        }
    }

    private void HighlightFirstInstructionSet()
    {
        instructionSetManagers[0].HighLightInstructionSetImage(true);
    }

    public FUNCTIONS GetCurrentSelectedInstructionSet()
    {
        return instructionSetManagers[currentIndexAt].GetFunctionData();
    }
}
