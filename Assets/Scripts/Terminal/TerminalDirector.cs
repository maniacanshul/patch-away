﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalDirector : MonoBehaviour
{
    public GameDirector gameDirector;
    public CodeDirector codeDirector;
    public AudioDirector audioDirector;
    public InstructionSetDirector instructionSetDirector;
    public CharacterDirector characterDirector;
    public CountInputFieldManager countInputFieldManager;
    public CodeToMovementInstructionsConverter codeToMovementInstructionsConverter;
    public GridDirector gridDirector;
    public InstructionsLaserDirector instructionsLaserDirector;

    private bool isCodeWindowActive = true;
    private bool canUploadCode = false;

    private void Start()
    {
        canUploadCode = true;
    }

    private void Update()
    {
        if (gameDirector.gameState != GAME_STATE.GAME)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space) && !countInputFieldManager.isInputFieldActive)
        {
            isCodeWindowActive = !isCodeWindowActive;
            if (isCodeWindowActive)
            {
                canUploadCode = false;
                countInputFieldManager.ShowCountPanel(true, instructionSetDirector.currentIndexAt);
                audioDirector.PlaySFX(AUDIO_SFX_EVENTS.INSTRUCTION_WINDOW_INTERACTION);
                instructionSetDirector.dontNavigateWhenInputtingNumbers = false;
                // instructionSetDirector.ToggleActive(false);
            }
            else
            {
                audioDirector.PlaySFX(AUDIO_SFX_EVENTS.CODE_WINDOW_INTERACTION);
                canUploadCode = false;
                codeDirector.ToggleActive(isCodeWindowActive);
                instructionSetDirector.ToggleActive(!isCodeWindowActive);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            audioDirector.PlaySFX(AUDIO_SFX_EVENTS.ESCAPE_KEY);
            if (countInputFieldManager.isInputFieldActive)
            {
                countInputFieldManager.ShowCountPanel(false, -1);
                isCodeWindowActive = false;
                codeDirector.ToggleActive(isCodeWindowActive);
                instructionSetDirector.ToggleActive(!isCodeWindowActive);
                canUploadCode = isCodeWindowActive;
            }
            else
            {
                isCodeWindowActive = true;
                codeDirector.ToggleActive(isCodeWindowActive);
                instructionSetDirector.ToggleActive(!isCodeWindowActive);
                canUploadCode = isCodeWindowActive;
            }
        }
        else if (Input.GetKeyDown(KeyCode.U) && canUploadCode && !characterDirector.isCharacterMoving)
        {
            UploadButtonCallback();
        }
        else if (Input.GetKeyDown(KeyCode.T) && canUploadCode)
        {
            SimulateButtonCallback();
        }
    }

    public void SetupNewFunctionValues(int repetition)
    {
        audioDirector.PlaySFX(AUDIO_SFX_EVENTS.NUMBER_INPUT);
        isCodeWindowActive = true;
        canUploadCode = isCodeWindowActive;
        codeDirector.SetupNewFunction(instructionSetDirector.GetCurrentSelectedInstructionSet(), repetition);
        codeDirector.ToggleActive(isCodeWindowActive);
        instructionSetDirector.ToggleActive(!isCodeWindowActive);
    }

    public void UploadButtonCallback()
    {
        audioDirector.PlaySFX(AUDIO_SFX_EVENTS.UPLOAD);
        instructionsLaserDirector.DrawLasers();
        codeToMovementInstructionsConverter.ConvertCodeToMovementInstructions(codeDirector.GetCurrentCode(), true);
        characterDirector.UploadCharacterMovement(codeToMovementInstructionsConverter.GetLastUploadedCode());
    }

    public void SimulateButtonCallback()
    {
        codeToMovementInstructionsConverter.ConvertCodeToMovementInstructions(codeDirector.GetCurrentCode(), false);
        gridDirector.UpdateGridBasedOnCurrentCode(codeToMovementInstructionsConverter.GetLastUploadedCode());
    }
}
