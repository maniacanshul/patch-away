﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class CountInputFieldManager : MonoBehaviour
{
    public TerminalDirector terminalDirector;
    public InputField inputField;

    internal bool isInputFieldActive;

    private PointerEventData pointerEventData;
    private RectTransform rectTransform;
    private float[] buttonPos = { -7, -97, -190 };

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        pointerEventData = new PointerEventData(EventSystem.current);
    }

    public void LoadLevel()
    {
        // inputField.enabled = false;
        ShowCountPanel(false, -1);
    }

    public void ShowCountPanel(bool show, int buttonPress)
    {
        transform.DOKill();
        isInputFieldActive = show;
        if (show)
        {
            rectTransform.DOAnchorPosY(buttonPos[buttonPress], 0);
            transform.DOLocalMoveX(0, 0.25f).OnComplete(() =>
       {
        //    inputField.enabled = true;
           inputField.text = "COUNT...";
           EventSystem.current.SetSelectedGameObject(show ? inputField.gameObject : null, null);
       });

        }
        else
        {
            transform.DOLocalMoveX(400, 0.25f).OnComplete(() =>
           {
            //    inputField.enabled = true;
               inputField.text = "COUNT...";
               EventSystem.current.SetSelectedGameObject(show ? inputField.gameObject : null, null);
           });
        }
    }

    public void ValueReceived()
    {
        if (inputField.text.Length < 1)
        {
            EventSystem.current.SetSelectedGameObject(inputField.gameObject, null);
            inputField.OnPointerClick(pointerEventData);
            return;
        }

        ShowCountPanel(false, -1);
        //To not read enter key for character movement on the same frame.
        Invoke("DelayInSettingUpFunction", 0.05f);
    }

    private void DelayInSettingUpFunction()
    {
        int inputFieldValue;
        if (int.TryParse(inputField.text, out inputFieldValue))
        {
            terminalDirector.SetupNewFunctionValues(int.Parse(inputField.text));
        }
    }
}
