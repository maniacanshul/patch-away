﻿public enum FUNCTIONS
{
    TURN_RIGHT,
    TURN_LEFT,
    WALK,
    NULL
}

public enum MOVEMENT_DIRECTION
{
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}

public struct CodeData
{
    public FUNCTIONS function;
    public int repetition;
}

public struct MovementData
{
    public MOVEMENT_DIRECTION direction;
    public bool isMovement;
}

public enum AUDIO_SFX_EVENTS
{
    CHARACTER_MOVEMENT,
    CHARACTER_ROTATION,
    CODE_WINDOW_INTERACTION,
    INSTRUCTION_WINDOW_INTERACTION,
    NUMBER_INPUT,
    ESCAPE_KEY,
    ARROW_KEYS,
    FUNCTION_DELETE,
    LEVEL_WON,
    UPLOAD,
    GAME_OVER
}

public enum GAME_STATE
{
    MAIN_MENU,
    GAME,
    LEVEL_OVER
}
