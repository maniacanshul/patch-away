﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GridDirector : MonoBehaviour
{
    public CharacterDirector characterDirector;
    public GameDirector gameDirector;
    public Image timerImage;
    public Text scoreText;
    public Sprite gridTargetPoint, gridDamage, baseGrid;
    public List<GridRowManager> gridRowManagers;

    internal Vector2Int targetPoint;
    internal List<Vector2> obstaclePoints;

    private Vector2 currentGridIndex;

    private Coroutine timerRoutine;
    private int timer = 0;
    private int levelCounter;

    public void LoadLevel()
    {
        levelCounter = 0;
        obstaclePoints = new List<Vector2>();
        DoGridLoadAnimation();
    }

    public void LoadNewTargetPoint()
    {
        RemoveHighlightFromAllGrid(true);
        do
        {
            targetPoint = new Vector2Int(Random.Range(0, 7), Random.Range(0, 7));
        } while (Vector2.Distance(characterDirector.transform.position, GetTargetPointCoordinates()) < 0.25f);

        gridRowManagers[targetPoint.y].SetupTargetPoint(targetPoint.x, gridTargetPoint);

        gameDirector.score += timer;
        scoreText.text = "SCORE: " + gameDirector.score.ToString();
        levelCounter++;
        timer = gameDirector.timer;
        timerImage.fillAmount = 1;
        SetupObstacle();
        if (timerRoutine != null)
        {
            StopCoroutine(timerRoutine);
        }
        timerRoutine = StartCoroutine(TimerCoroutineIE());
    }

    private void SetupObstacle()
    {
        if (levelCounter < 5)// Test
        {
            return;
        }

        obstaclePoints.Clear();
        Vector2 tempObs = Vector2.zero;
        for (int i = 0; i < Random.Range(1, 4); i++)
        {
            do
            {
                tempObs = new Vector2Int(Random.Range(0, 7), Random.Range(0, 7));
            } while (Vector2.Distance(characterDirector.transform.position, GetTargetObstaclePointCoordinates(tempObs)) < 0.25f || tempObs == targetPoint);
            obstaclePoints.Add(tempObs);
            gridRowManagers[(int)tempObs.y].SetupObstaclePoint((int)tempObs.x);
        }
    }

    private IEnumerator TimerCoroutineIE()
    {
        timerImage.DOKill();
        timerImage.DOFillAmount(0, timer).SetEase(Ease.Linear);
        while (timer > 0)
        {
            yield return new WaitForSeconds(1);
            timer--;
        }
        gameDirector.GameOver();
        ResetLevel();
    }

    private void ResetLevel()
    {
        for (int i = 0; i < gridRowManagers.Count; i++)
        {
            gridRowManagers[i].DoGridLoadAnimation(false);
        }
        RemoveHighlightFromAllGrid(false);
    }

    public void UpdateGridBasedOnCurrentCode(List<MovementData> codeData)
    {
        currentGridIndex = CharacterOnThisGridCell();
        RemoveHighlightFromAllGrid(false);
        for (int i = 0; i < codeData.Count; i++)
        {
            if (codeData[i].isMovement)
            {
                switch (codeData[i].direction)
                {
                    case MOVEMENT_DIRECTION.TOP:
                        {
                            currentGridIndex.y--;
                            break;
                        }
                    case MOVEMENT_DIRECTION.BOTTOM:
                        {
                            currentGridIndex.y++;
                            break;
                        }
                    case MOVEMENT_DIRECTION.LEFT:
                        {
                            currentGridIndex.x--;
                            break;
                        }
                    case MOVEMENT_DIRECTION.RIGHT:
                        {
                            currentGridIndex.x++;
                            break;
                        }
                }
                bool obstaclePresent = false;
                for (int j = 0; j < obstaclePoints.Count; j++)
                {
                    if (obstaclePoints[j] == currentGridIndex)
                    {
                        obstaclePresent = true;
                        break;
                    }
                }
                if (!obstaclePresent)
                {
                    HighLightGrid();
                }
                else
                {
                    break;
                }
            }
        }
    }


    private void RemoveHighlightFromAllGrid(bool resetGridImages)
    {
        for (int i = 0; i < gridRowManagers.Count; i++)
        {
            gridRowManagers[i].ResetAllGridHighlights(resetGridImages ? baseGrid : null);
        }
    }

    private void HighLightGrid()
    {
        currentGridIndex.x = Mathf.Clamp(currentGridIndex.x, 0, 6);
        currentGridIndex.y = Mathf.Clamp(currentGridIndex.y, 0, 6);
        gridRowManagers[(int)currentGridIndex.y].HighLightGridCell((int)currentGridIndex.x);
    }

    private void DoGridLoadAnimation()
    {
        for (int i = 0; i < gridRowManagers.Count; i++)
        {
            gridRowManagers[i].DoGridLoadAnimation(true);
        }
    }

    private Vector2 GetTargetObstaclePointCoordinates(Vector2 obstaclePoint)
    {
        Transform gridCell = gridRowManagers[(int)obstaclePoint.y].gridCells[(int)obstaclePoint.x].transform;
        return gridCell.position;
    }

    public Vector2 GetTargetPointCoordinates()
    {
        Transform gridCell = gridRowManagers[targetPoint.y].gridCells[targetPoint.x].transform;
        return gridCell.position;
    }

    private Vector2 CharacterOnThisGridCell()
    {
        Vector2 characterOnThis = Vector2.zero;
        for (int i = 0; i < gridRowManagers.Count; i++)
        {
            if (Mathf.Abs(characterDirector.transform.position.y - gridRowManagers[i].transform.position.y) < 0.25f)
            {
                characterOnThis.y = i;
                for (int j = 0; j < gridRowManagers[i].gridCells.Count; j++)
                {
                    if (Mathf.Abs(characterDirector.transform.position.x - gridRowManagers[i].gridCells[j].transform.position.x) < 0.25f)
                    {
                        characterOnThis.x = j;
                        break;
                    }
                }
                break;
            }
        }
        return characterOnThis;
    }

    Vector2 checkGridCell;
    public bool CheckIfNextGridIsObstacleForCharacter(MOVEMENT_DIRECTION direction)
    {
        bool isObstacle = false;

        checkGridCell = CharacterOnThisGridCell();
        switch (direction)
        {
            case MOVEMENT_DIRECTION.BOTTOM:
                {
                    checkGridCell.y++;
                    break;
                }
            case MOVEMENT_DIRECTION.TOP:
                {

                    checkGridCell.y--;
                    break;
                }
            case MOVEMENT_DIRECTION.LEFT:
                {

                    checkGridCell.x--;
                    break;
                }
            case MOVEMENT_DIRECTION.RIGHT:
                {

                    checkGridCell.x++;
                    break;
                }

        }

        checkGridCell.x = Mathf.Clamp(checkGridCell.x, 0, 6);
        checkGridCell.y = Mathf.Clamp(checkGridCell.y, 0, 6);

        for (int i = 0; i < obstaclePoints.Count; i++)
        {
            if (obstaclePoints[i] == checkGridCell)
            {
                isObstacle = true;
                break;
            }
        }

        return isObstacle;
    }
}
