﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GridRowManager : MonoBehaviour
{
    public List<SpriteRenderer> gridCells;

    private int indexTarget;

    public void HighLightGridCell(int index)
    {
        gridCells[index].color = Color.red;
    }

    public void ResetAllGridHighlights(Sprite baseGridSprite = null)
    {
        for (int i = 0; i < gridCells.Count; i++)
        {
            if (i != indexTarget)
            {
                gridCells[i].color = Color.white;
            }
            if (baseGridSprite != null)
            {
                indexTarget = -1;
                gridCells[i].color = Color.white;
                gridCells[i].sprite = baseGridSprite;
            }
        }
    }

    public void SetupTargetPoint(int index, Sprite sprite)
    {
        indexTarget = index;
        gridCells[index].sprite = sprite;
        gridCells[index].color = Color.green;
    }

    public void SetupObstaclePoint(int index)
    {
        gridCells[index].sprite = null;
    }

    public void DoGridLoadAnimation(bool showUp)
    {
        for (int i = 0; i < gridCells.Count; i++)
        {
            gridCells[i].transform.DOScale(Vector2.zero, 0);
            if (showUp)
            {
                gridCells[i].transform.DOScale(Vector2.one, 0.5f).SetDelay(Random.Range(0.25f, 0.75f)).SetEase(Ease.OutBack);
            }
        }
    }
}
